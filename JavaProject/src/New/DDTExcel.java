package New;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DDTExcel {

	ChromeDriver driver;
	
	@Test(dataProvider = "loginpositivecasedata")
	public void LoginPositiveCases(String username, String password) 
	{
		
		try {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karthikeyan\\eclipse-workspace\\JavaProject\\lib\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get("https://testing-fdcrc.bascii.com/");
			driver.manage().window().maximize();
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("#txtusername")).sendKeys(username);
			driver.findElement(By.cssSelector("#txtPassword")).sendKeys(password);
			driver.findElement(By.cssSelector("button.btn.btn.btn-primary.btn-lg")).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertEquals(Actual, Expected);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test(dataProvider = "loginnegativecasedata")
	public void LoginNegativeCases(String username, String password) 
	{	
		try {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karthikeyan\\eclipse-workspace\\JavaProject\\lib\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get("https://testing-fdcrc.bascii.com/");
			driver.manage().window().maximize();	
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("#txtusername")).sendKeys(username);
			driver.findElement(By.cssSelector("#txtPassword")).sendKeys(password);
			driver.findElement(By.cssSelector("button.btn.btn.btn-primary.btn-lg")).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertNotEquals(Actual, Expected);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	void ProgranTermination() 
	{
		
		driver.quit();
		
	}
	
	@DataProvider(name="loginnegativecasedata")
	public Object[][] LoginNegativeFeed()
	{
		
		 ReadExcelFile config = new ReadExcelFile("C:\\Users\\Karthikeyan\\Desktop\\Login.xlsx");
		
		 int rows = config.getRowCount(0);
		 
		 Object[][] credentials = new Object[rows][2];
		 
		 for(int i=0;i<rows;i++) 
		 {
			 credentials[i][0] = config.getData(0,i,0);
			 credentials[i][1] = config.getData(0,i,1);
			 
		 }
		 
		 return credentials;
	}
	
	@DataProvider(name="loginpositivecasedata")
	public Object[][] LoginPositiveFeed()
	{
		
		 ReadExcelFile config = new ReadExcelFile("C:\\Users\\Karthikeyan\\Desktop\\Login.xlsx");
		
		 int rows = config.getRowCount(1);
		 
		 Object[][] credentials = new Object[rows][2];
		 
		 for(int i=0;i<rows;i++) 
		 {
			 credentials[i][0] = config.getData(1,i,0);
			 credentials[i][1] = config.getData(1,i,1);
			 
		 }
		 
		 return credentials;
	}
	
	
	
}
