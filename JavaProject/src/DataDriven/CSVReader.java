package DataDriven;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

public class CSVReader {
	
//	static String BaseURL = "https://testing-fdcrc.bascii.com/";
//	static String CSV_Path = "C:\\Users\\Karthikeyan\\Desktop\\Test.csv";

	public static HashMap<String, String> getData(String CSV_Path) throws ParseException, IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(CSV_Path));
	    String line =  null;
	    HashMap<String,String> map = new HashMap<String, String>();

	    while((line=br.readLine())!=null){
	        String str[] = line.split(",");
	        map.put(str[0], str[1]);
	    }
	    
	    return map;

	}

}
