package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;



public class UserPage {
	
	By users = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[1]/div[1]/div[9]/div[1]/div[1]/div[2]/a[1]");
	By createuserbtn = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/button[1]");
	By fname = By.cssSelector("#input-default");
	By lname = By.cssSelector("#input-surname");
	By username1 = By.cssSelector("#input-username");
	By password1 = By.cssSelector("#input-password");
	By cpassword = By.cssSelector("#input-confirmpassword");
	By email = By.cssSelector("#input-emailAddress");
	By userroles = By.cssSelector("div > div:nth-of-type(2) > label.custom-control-label");
	By isactive = By.cssSelector("fieldset.form-group > div > div.custom-control.custom-checkbox > label.custom-control-label");
	By submit = By.cssSelector("button.btn.btn-primary");
	By filter = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/input[1]");
	By nameselect = By.linkText("anila");
	By nfname = By.id("input-default");
	By nlname = By.id("input-surname");
	By nemail = By.id("input-email");
	By nsave = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/form[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[2]/button[1]");
	By deleteuser = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/form[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[2]/button[3]");
	By deletestage = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/button[1]");
	By docupload = By.xpath("//*[@id=\"sidebar\"]/div/div/div[9]/div/div/div[6]");
	By letterdate = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]");
	By dateselection = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[3]/div[1]");
	By lettertype = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[2]/div[2]/select[1]");
	By city = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[3]/div[2]/select[1]");
	By state = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[4]/div[2]/select[1]");
	By client = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[5]/div[2]/select[1]");
	By originalcreditor = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[2]/div[2]/select[1]");
	By recentcreditor = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[3]/div[2]/select[1]");
	By collectionagency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[4]/div[2]/select[1]");
	By fileupload = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[2]/div[1]/div[1]/input[1]");
	

	
	public void UserCreateTC1(WebDriver driver) {
		
			try {
				Thread.sleep(1500);
				driver.findElement(users).click();
				driver.findElement(createuserbtn).click();
				driver.findElement(fname).sendKeys("anila");
				driver.findElement(lname).sendKeys("anila");
				driver.findElement(email).sendKeys("as@dd.com");
				driver.findElement(username1).sendKeys("anila");
				driver.findElement(password1).sendKeys("anila");
				driver.findElement(cpassword).sendKeys("anila");
				driver.findElement(isactive).click();
				driver.findElement(userroles).click();
				driver.findElement(submit).click();
				Thread.sleep(1000);
				String Actual = driver.getTitle();
				System.out.println(Actual);
				String Expected = "Create User - FDCRA";
				System.out.println(Expected);
				Assert.assertEquals(Actual, Expected);
				System.out.println("User Creation Successful");
				
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				
	}
	
	
	public void UserupdateTC2(WebDriver driver) {
		
	
		try {
			Thread.sleep(1500);
			driver.findElement(users).click();
			driver.findElement(filter).sendKeys("anila");
			Thread.sleep(1500);
			driver.findElement(nameselect).click();
			driver.findElement(nfname).clear();
			driver.findElement(nfname).sendKeys("akil");
			driver.findElement(nlname).clear();
			driver.findElement(nlname).sendKeys("sADSd");
			Thread.sleep(1500);
			driver.findElement(nemail).clear();
			driver.findElement(nemail).sendKeys("addSDD@SFA.com");
			Thread.sleep(1500);
			driver.findElement(nsave).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "Edit User - FDCRA";
			System.out.println(Expected);
			Assert.assertEquals(Actual, Expected);
			System.out.println("User Updation Successful");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	
	public void UserdeleteTC3(WebDriver driver) {
		
		try {
			Thread.sleep(1500);
			driver.findElement(users).click();
			Thread.sleep(1500);
			driver.findElement(filter).sendKeys("anila");
			Thread.sleep(1500);
			driver.findElement(nameselect).click();
			Thread.sleep(1500);
			driver.findElement(deleteuser).click();
			Thread.sleep(1500);
			driver.findElement(deletestage).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "Edit User - FDCRA";
			System.out.println(Expected);
			Assert.assertEquals(Actual, Expected);
			System.out.println("User Deletion Successful");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
		
	
		
		
		
		
		
	}
	

