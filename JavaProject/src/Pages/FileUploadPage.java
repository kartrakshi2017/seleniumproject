package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class FileUploadPage {
	
	
	By docupload = By.xpath("//*[@id=\"sidebar\"]/div/div/div[9]/div/div/div[6]");
	By letterdate = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[1]/div[2]/div[1]/input[1]");
	By dateselection = By.xpath("//*[@id=\"letterDate\"]/div/div/table/tbody/tr[5]/td[4]/div");
	By lettertype = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[2]/div[2]/select[1]");
	By city = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[3]/div[2]/select[1]");
	By state = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[4]/div[2]/select[1]");
	By client = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[5]/div[2]/select[1]");
	By originalcreditor = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[2]/div[2]/select[1]");
	By recentcreditor = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[3]/div[2]/select[1]");
	By collectionagency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[2]/div[4]/div[2]/select[1]");
	By fileupload = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[2]/div[1]/div[1]/input[1]");
	By loadingfile = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[3]/div[1]/button[1]");
	
	ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/upload.html");
	ExtentReports extent = new ExtentReports();
	
	public void documentupload(WebDriver driver) {
		this.extent.attachReporter(this.reporter);
		ExtentTest logger = this.extent.createTest("documentupload");	
		
		try {
			Thread.sleep(1000);
			logger.log(Status.INFO, "Document Upload - FDCRA");
			driver.findElement(docupload).click();
			driver.findElement(letterdate).click();
			Thread.sleep(1000);
			driver.findElement(dateselection).click();
			Thread.sleep(1000);
			WebElement element1 = driver.findElement(lettertype);
			Select select1 = new Select(element1);
			select1.selectByValue("12");
			Thread.sleep(1000);
			WebElement element2 = driver.findElement(city);
			Select select2 = new Select(element2);
			select2.selectByValue("827");
			Thread.sleep(1000);
			WebElement element3 = driver.findElement(state);
			Select select3 = new Select(element3);
			select3.selectByValue("12");
			Thread.sleep(1000);
			WebElement element4 = driver.findElement(client);
			Select select4 = new Select(element4);
			select4.selectByValue("1");
			Thread.sleep(1000);
			WebElement element5 = driver.findElement(originalcreditor);
			Select select5 = new Select(element5);
			select5.selectByValue("22");
			Thread.sleep(1000);
			WebElement element6 = driver.findElement(recentcreditor);
			Select select6 = new Select(element6);
			select6.selectByValue("5");
			Thread.sleep(1000);
			WebElement element7 = driver.findElement(collectionagency);
			Select select7 = new Select(element7);
			select7.selectByValue("1");
			Thread.sleep(2000);
			WebElement uploadElement = driver.findElement(fileupload);
			uploadElement.sendKeys("C:\\Users\\Karthikeyan\\Desktop\\dummy.pdf");
			Thread.sleep(1000);
			driver.findElement(loadingfile).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "Document Upload - FDCRA";
			System.out.println(Expected);
			Assert.assertEquals(Actual, Expected);
			System.out.println("fileupload Success");	
			logger.log(Status.PASS, "Document Upload - FDCRA");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AssertionError e) {
			System.out.println("Uploaddocument Failed");
			logger.log(Status.FAIL, "Document Upload - FDCRA");
		}
		
		
	}
		

}
