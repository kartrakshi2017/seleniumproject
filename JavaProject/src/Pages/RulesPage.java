package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RulesPage {
	
	By rulesclick = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/nav[1]/div[1]/div[1]/div[9]/div[1]/div[1]/div[1]");
	By rulesname = By.id("input-default");
	By ruleschoosefield = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[1]/p[1]/div[1]/div[2]/fieldset[1]/div[1]/select[1]");
	By rulescity = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[1]/p[1]/div[3]/div[2]/div[2]/fieldset[1]/div[1]/select[1]/option[2]");
	By rulesforward = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[1]/p[1]/div[3]/div[2]/div[3]/a[1]");
	By textinsert = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/div[1]/p[1]/div[1]/div[2]/textarea[1]");
	By textinsertclick = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/div[1]/p[1]/div[1]/div[3]/button[1]");
	By textinsert1 = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/div[1]/p[1]/div[3]/div[2]/textarea[1]");
	By shortmessage = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/div[1]/p[1]/div[5]/div[2]/textarea[1]");
	By longmessage = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/div[1]/p[1]/div[6]/div[2]/textarea[1]");
	By saverules = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset[1]/div[1]/div[1]/div[1]/button[1]");
	
	
public void rulestest(WebDriver driver) {
	
	try {
		Thread.sleep(1500);
		driver.findElement(rulesclick).click();
		Thread.sleep(1000);
		driver.findElement(rulesname).sendKeys("newrules");
		Thread.sleep(1000);
		WebElement element1 = driver.findElement(ruleschoosefield);
		Select select1 = new Select(element1);
		select1.selectByValue("City");
		Thread.sleep(1000);
		driver.findElement(rulescity).click();
		driver.findElement(rulesforward).click();
		driver.findElement(textinsert).sendKeys("Done");
		driver.findElement(textinsertclick).click();
		driver.findElement(shortmessage).sendKeys("Done");
		driver.findElement(longmessage).sendKeys("Done");
		driver.findElement(saverules).click();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}



}