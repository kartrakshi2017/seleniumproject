package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LoginPage {
	
	By username = By.cssSelector("#txtusername");
	By password = By.cssSelector("#txtPassword");
	By submit = By.cssSelector("button.btn.btn.btn-primary.btn-lg");
	
	ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/TestResults.html");
	ExtentReports extent = new ExtentReports();
	
	public void loginsTC1(WebDriver driver) {
		this.extent.attachReporter(this.reporter);
		ExtentTest logger = this.extent.createTest("loginsTC1");
		
		try {
			logger.log(Status.INFO, "Login to FDCRA");
			Thread.sleep(500);
			driver.findElement(username).sendKeys("testadmin");
			driver.findElement(password).sendKeys("QKEMzzs4");
			driver.findElement(submit).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertEquals(Actual, Expected);
			System.out.println("User_TC1 Success with Rightvalues");	
			logger.log(Status.PASS, "Login to FDCRA");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AssertionError e) {
			System.out.println("Login_TC1 Failed");
			logger.log(Status.FAIL, "User to FDCRA");
		}
				
	}
	
	public void loginsTC2(WebDriver driver) {
		
		this.extent.attachReporter(this.reporter);
		ExtentTest logger = this.extent.createTest("loginsTC2");
		
		try {
			Thread.sleep(500);
			logger.log(Status.INFO, "Login to FDCRA");
			driver.findElement(username).sendKeys("testadmi");
			driver.findElement(password).sendKeys("QKEMzzs4");
			driver.findElement(submit).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertNotEquals(Actual, Expected);
			System.out.println("Login_TC2 Success with Wrongusername and Right Password- Hence Unable to Login");	
			logger.log(Status.PASS, "Login to FDCRA");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AssertionError e) {
			System.out.println("Login_TC2 Failed");
			logger.log(Status.FAIL, "Login to FDCRA");
		}
				
	}
	
	public void loginsTC3(WebDriver driver) {
		this.extent.attachReporter(this.reporter);
		ExtentTest logger = this.extent.createTest("loginsTC3");
		
		try {
			Thread.sleep(500);
			logger.log(Status.INFO, "Login to FDCRA");
			driver.findElement(username).sendKeys("testadmin");
			driver.findElement(password).sendKeys("QKEMzzs");
			driver.findElement(submit).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertNotEquals(Actual, Expected);
			System.out.println("Login_TC3 Success with Rightusername and Wrong Password- Hence Unable to Login");
			logger.log(Status.PASS, "Login to FDCRA");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AssertionError e) {
			System.out.println("Login_TC3 Failed");
			logger.log(Status.FAIL, "Login to FDCRA");
		}
				
	}
	
	public void loginsTC4(WebDriver driver) {
		this.extent.attachReporter(this.reporter);
		ExtentTest logger = this.extent.createTest("loginsTC4");
		try {
			Thread.sleep(500);
			logger.log(Status.INFO, "Login to FDCRA");
			driver.findElement(username).sendKeys("");
			driver.findElement(password).sendKeys("QKEMzzs4");
			driver.findElement(submit).click();
			Thread.sleep(1000);
			String Actual = driver.getTitle();
			System.out.println(Actual);
			String Expected = "FDCRA";
			System.out.println(Expected);
			Assert.assertNotEquals(Actual, Expected);
			System.out.println("Login_TC4 Success without Username and Right Password- Hence Unable to Login");	
			logger.log(Status.PASS, "Login to FDCRA");
			this.extent.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			this.extent.flush();
			e.printStackTrace();
		} catch (AssertionError e) {
			System.out.println("Login_TC4 Failed");
			logger.log(Status.FAIL, "Login to FDCRA");
			this.extent.flush();
			
		}
				
	}
	

}
