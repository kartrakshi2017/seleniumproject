package ExtentReports;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportExtent {
	
	@Test
	public void logintest() 
	{
		
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/SampleReport.html");
		ExtentReports extent = new ExtentReports();
		
		System.out.println("Login to FDCRA");
		
		extent.attachReporter(reporter);
		ExtentTest logger = extent.createTest("loginsTC1");
		
		logger.log(Status.INFO, "Login to FDCRA");
		
		logger.log(Status.PASS, "Login to FDCRA");
		
		extent.flush();
		
		
		
		
	}

}
