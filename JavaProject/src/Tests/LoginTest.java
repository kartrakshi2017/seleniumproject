package Tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Browser.Confiq;
import Pages.FileUploadPage;
import Pages.LoginPage;
import Pages.RulesPage;
import Pages.UserPage;


public class LoginTest {
	
	WebDriver driver;
	Confiq con = new Confiq();
	LoginPage login = new LoginPage();
	UserPage user = new UserPage();
	FileUploadPage files = new FileUploadPage();
	RulesPage rules = new RulesPage();
	
	@BeforeMethod
	public void beforeclass() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karthikeyan\\eclipse-workspace\\JavaProject\\lib\\chromedriver.exe");
		this.driver = new ChromeDriver();
		this.driver.get("https://testing-fdcrc.bascii.com/");
		this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		this.driver.manage().window().maximize();
	}
	
	@Test(priority=1, enabled=true)
	public void logintest1() {
		
		login.loginsTC1(this.driver);
		
	}
	
	@Test(priority=2, enabled=true)
	public void logintest2() {
		
		login.loginsTC2(this.driver);
		
	}
	
	@Test(priority=3, enabled=true)
	public void logintest3() {
		
		login.loginsTC3(this.driver);
		
	}
	
	@Test(priority=4, enabled=true)
	public void logintest4() {
		
		login.loginsTC4(this.driver);
		
	}
		
	@AfterMethod
	public void afterclass() {
		
		driver.quit();
		
	}
	

}
