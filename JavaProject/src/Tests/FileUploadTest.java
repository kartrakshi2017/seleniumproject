package Tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.FileUploadPage;
import Pages.LoginPage;

	public class FileUploadTest {
	
	WebDriver driver;
	FileUploadPage fileupload = new FileUploadPage();
	LoginPage log = new LoginPage();
	By username = By.cssSelector("#txtusername");
	By password = By.cssSelector("#txtPassword");
	By submit = By.cssSelector("button.btn.btn.btn-primary.btn-lg");
	

		@BeforeMethod
		public void beforeclass() 
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karthikeyan\\eclipse-workspace\\JavaProject\\lib\\chromedriver.exe");
			this.driver = new ChromeDriver();
			this.driver.get("https://testing-fdcrc.bascii.com/");
			this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			this.driver.manage().window().maximize();
			driver.findElement(username).sendKeys("testadmin");
			driver.findElement(password).sendKeys("QKEMzzs4");
			driver.findElement(submit).click();
		}

		@Test(priority=1, enabled=true)
		public void createuserTC1() 
		{
			fileupload.documentupload(this.driver);
		}
		
		@AfterMethod
		public void afterclass() 
		{
			driver.quit();
		}





}