package Tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.LoginPage;
import Pages.RulesPage;

public class RulesTest {
	
	WebDriver driver;
	RulesPage rules = new RulesPage();
	LoginPage logi = new LoginPage();

		@BeforeMethod
		public void beforeclass() 
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karthikeyan\\eclipse-workspace\\JavaProject\\lib\\chromedriver.exe");
			this.driver = new ChromeDriver();
			this.driver.get("https://testing-fdcrc.bascii.com/");
			this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			this.driver.manage().window().maximize();
			logi.loginsTC1(this.driver);
		}

		@Test(priority=1, enabled=true)
		public void createuserTC1() 
		{
			rules.rulestest(this.driver);
		}
		
		@AfterMethod
		public void Afterclass() 
		{
			
			driver.quit();
		}


}
